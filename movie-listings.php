<?php
/*Plugin Name: Movie Listings */

//Front end Scripts
require_once("movie-listing-scripts.php");

//Admin scripts

require_once("movie-listing-cpt.php");
require_once("movie-listing-settings.php");
require_once("movie-listing-fields.php");
require_once("movie-listing-reorder.php");