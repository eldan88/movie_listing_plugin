<?php


function ml_list_movies($atts,$content){

    $atts = shortcode_atts(array(

        "title" => "Latest Movies",
        "count" => 12,
        "genre" => "all",
        "pagination" => "off"


    ),$atts);


    $pagination = $atts["pagination"] == "on" ? false : true;
    $paged = get_query_var("paged") ? get_query_var("paged") : 1 ;



    if($atts["genre"] == "all"){

        $terms = "";
    } else {

         $terms = array(array(

                 "taxonomy"=>"genres",
                  "field"=>"slug",
                 "terms"=> $atts["genre"]



         ));

    }


    $args = array(

        "post_type" => "movie_listing",
        "post_status" => "publish",
        "orderby" => "menu_order",
        "order" => "ACS",
        "no_found_rows"=> $pagination,
        "posts_per_page"=> $atts["count"],
        "paged"=> $paged,
        "tax_query"=>$terms



    );



    $movies = new WP_Query($args);


    //Check for movies

    if($movies->have_posts()){

        $genre = str_replace("-"," ",$atts["genre"]);

        $output = "<div class='movie-list'>";

        while($movies->has_posts()){

            $movies->the_post();

            $image = wp_get_attachment_image_src(get_post_thumbnail_id(),"single-post-thumbnail");

            $output .= "<div class='movie-col'>";

            $output .= "<img class='feat-image' src='$image[0]'  >";
            $output .= "<h5 class='movie-title'> get_the_title() </h5>";
            $output .= "<a href='".get_the_permalink()."'>View Details</a>";


            $output .= "</div>";

        }

        wp_reset_postdata();


        $output .= "</div>";

        return $output;

    } else {
        return "<p>No Movies Found</p>";
    }
}

add_shortcode("movies","ml_list_movies");
