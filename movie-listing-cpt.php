<?php




function movie_listing(){

   $singular = "Movie Listing";
    $plural = "Movie Listings";

    $labels = array(

        "name" => $singular,
        "singular_name" => $plural,
        "add_new" => "Add New",
        "add_new_item" =>"Add New ". $singular,
        "edit" => "Edit",
        "edit_item" => "Edit ". $singular,
        "new_item" => "New ". $singular,
        "view" => "View",
        "view_item" => "View" . $singular,
        "search_items" => "Search" . $plural,
        "not_found" => "No  $plural Found",
        "not_found_in_trash" =>"No  $plural Found",
        "parent_item_colon" => "Parent " . $singular,
        "menu_name" => $plural



    );


    $args = array(


        "labels" => $labels,
        "hierarchical" => true,
        "description" => "Movie listing by genere",
        "taxonomies" => array("genre"),
        "public" => true,
        "show_ui"=>true,
        "show_in_menu"=>true,
        "menu_position"=>5,
        "menu_icon"=>"dash-video-alt2",
        "show_in_nav_menu"=>true,
        "public_queryable"=>true,
        "exclude_from_search"=>false,
        "has_archive"=>true,
        "query_var"=>true,
        "can_export"=>true,
        "rewrite"=>true,
        "capability_type"=>"post",
        "supports"=>array(
            "title",
            "thumbnail",


        )



    );



    register_post_type("movie_listing",$args);



}

add_action("init","movie_listing");


function genre_taxonomy(){


    register_taxonomy("genres","movie_listing",array(

        "label"=>"Genres",
        "query_var"=> true,
        "rewrite"=>array(

            "slug"=>"genre",
            "with_font"=>false

        )



    ));


}

    add_action("init","genre_taxonomy");
