<?php


function movieListingSettings(){


    add_settings_section("ml_settings_section","Movie Listing Settings","ml_settings_section_callback","reading");
    add_settings_field("ml_setting_show_editor","Show Editor","show_editor_callback","reading","ml_settings_section");
    add_settings_field("ml_setting_show_media_buttons","Show Media Buttons","show_media_button_callback","reading","ml_settings_section");


    register_setting("reading","show_editor");
    register_setting("reading","show_media_buttons");



}

add_action("admin_init","movieListingSettings");

/**
 * Content
 */

function ml_settings_section_callback(){

    echo "<h4>Hi</h4>";
}


function show_editor_callback(){


    $show_editor_option = get_option("show_editor");

    echo "<input name='show_editor' id='show_editor' type='checkbox' value='1' ".checked(1,$show_editor_option,false)." > ";


}


function show_media_button_callback(){

 $show_media_buttons = get_option("show_media_buttons");

    echo "<input name='show_media_buttons' id='show_media_buttons' type='checkbox' value='1' ".checked(1,$show_media_buttons,false)." > ";


}