<?php


function add_field_meta_box(){

    add_meta_box(
        "listing_info",
        "Listing Info",
        "add_fields_call_back",
        "movie_listing",
        "normal",
        "default"
    );


}

add_action("add_meta_boxes","add_field_meta_box");


function add_fields_call_back($post){


    $mtl_stored_meta = get_post_meta($post->id);

    ?>


    <div class='wrap movie-listing-form'>

        <div class='form-group'>
            <label for='mpaa-rating'> Movie id </label>
            <input type='text' name='movie_id' value='<?php if(!empty($mtl_stored_meta['movie_id'])) { echo $mtl_stored_meta['movie_id'][0]; } ?>'>

        </div>


      <div class='form-group'>
            <label for='movie-id'> Mpaa Rating </label>

          <select name='mpaa_rating' id='mpaa_rating'>

              <option <?php  if($mtl_stored_meta['mpaa_rating'][0] == "G") {echo "selected"; } ?>>G</option>
              <option <?php  if($mtl_stored_meta['mpaa_rating'][0] == "PG") {echo "selected"; } ?>>PG</option>
              <option <?php  if($mtl_stored_meta['mpaa_rating'][0] == "PG-13") {echo "selected"; } ?>>PG-13</option>
              <option <?php  if($mtl_stored_meta['mpaa_rating'][0] == "PG-13") {echo "selected"; }?>> R</option>
              <option <?php  if($mtl_stored_meta['mpaa_rating'][0] == "NR") { echo "selected"; }?>>NR</option>


          </select>

        </div>



        <!--Show the editor only if the show editor is marked true on the get settings-->
        <?php if(get_settings("show_editor")) { ?>
          <div class='form-group'>
            <label for='details'> Details </label>

              <?php
              $content = get_post_meta($post->id,'details',TRUE);
              $editor = "details";
              $settings = array(
                  "textarea_rows"=>5,
                  "media_buttons"=> get_settings("show_media_buttons"),

              );

              wp_editor($content,$editor,$settings);

              ?>
        </div>

        <?php } else {  ?>

        <div class='form-group'>

            <label for='details'> Details</label>

            <textarea name='details' id='details'><?php if(!empty($mtl_stored_meta['details'])) { echo $mtl_stored_meta['details'][0]; } ?></textarea>


        </div>

        <?php } ?>


          <div class='form-group'>
            <label for='release_date'> Release   Date </label>
            <input type='date' id='release_date' name='release_date' value='<?php if(!empty($mtl_stored_meta['release_date'])) { echo $mtl_stored_meta['movie_id'][0]; } ?>'>

        </div>






    <div class='form-group'>
            <label for='director'> Director </label>
            <input type='date' id='release_date' name='release_date' value='<?php if(!empty($mtl_stored_meta['director'])) { echo $mtl_stored_meta['director'][0]; } ?>'>

        </div>



    <div class='form-group'>
            <label for='stars'> Stars </label>
            <input type='date' id='stars' name='release_date' value='<?php if(!empty($mtl_stored_meta['stars'])) { echo $mtl_stored_meta['stars'][0]; } ?>'>

        </div>



    <div class='form-group'>
            <label for='runtime'> Rumtime </label>
            <input type='date' id='runtime' name='runtime' value='<?php if(!empty($mtl_stored_meta['runtime'])) { echo $mtl_stored_meta['runtime'][0]; } ?>'>

        </div>




    <div class='form-group'>
            <label for='youtube-id'> Youtube ID/ Trailor </label>
            <input type='text' id='runtime' name='trailor' value='<?php if(!empty($mtl_stored_meta['trailor'])) { echo $mtl_stored_meta['trailor'][0]; } ?>'>

        </div>



   </div>

<?php






}


function save($post_id){


    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision =  wp_is_post_revision($post_id);


    if($is_autosave || $is_revision){
        return false;
    }



    if(isset($_POST['movie_id'])){

        update_post_meta($post_id,"movie_id",$_POST['movie_id']);

    }

    if(isset($_POST['mpaa_rating'])){

        update_post_meta($post_id,"mpaa_rating",$_POST['mpaa_rating']);

    }

    if(isset($_POST['release_date'])){

        update_post_meta($post_id,"details",$_POST['release_date']);

    }

    if(isset($_POST['director'])){

        update_post_meta($post_id,"director",$_POST['director']);

    }

     if(isset($_POST['stars'])){

        update_post_meta($post_id,"stars",$_POST['stars']);

    }


      if(isset($_POST['runtime'])){

        update_post_meta($post_id,"runtime",$_POST['runtime']);

    }

     if(isset($_POST['trailor'])){

        update_post_meta($post_id,"trailor",$_POST['trailor']);

    }





}


add_action("save_post","save");